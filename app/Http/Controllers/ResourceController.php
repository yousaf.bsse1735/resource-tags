<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use Inertia\Inertia;
use App\Models\Resource;
use App\Http\Requests\StoreResourceRequest;
use App\Http\Requests\UpdateResourceRequest;

class ResourceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return Inertia::render('Resources', [
            'resources' => Resource::with('tags')->get(),
            'tags' => Tag::orderby('title')->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreResourceRequest $request)
    {
        $resource = Resource::create([
            'title' => $request->title
        ]);

        $resource = Resource::with('tags')->where('id', $resource->id)->get()->first();
        return response()->json($resource);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateResourceRequest $request, Resource $resource)
    {
        $resource->update([
            'title' => $request->title
        ]);

        $resource = Resource::find($resource->id);

        return response()->json($resource);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Resource $resource)
    {
        foreach ($resource->tags as $tag) {
            if (count($tag->resources) == 1) {
                $tag->delete();
            }
        }
        $resource->delete();
        
        return response()->json(['message' => 'Resource deleted Successfully']);
    }
}

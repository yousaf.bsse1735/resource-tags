<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use App\Models\ResourceTag;
use Illuminate\Http\Request;
use App\Http\Requests\StoreTagRequest;
use App\Http\Requests\UpdateTagRequest;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreTagRequest $request)
    {
        $tag = Tag::updateOrCreate(
            ['title' => $request->title]
        );

        ResourceTag::updateOrCreate(
            ['resource_id' => $request->resource_id, 'tag_id' => $tag->id]
        );

        return response()->json($tag);
    }

    /**
     * Display the specified resource.
     */
    public function show(Tag $tag)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Tag $tag)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateTagRequest $request, Tag $tag)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
        $resource_tags = ResourceTag::where(
            ['resource_id' => $request->resource_id, 'tag_id' => $request->tag_id]
        )->get();
        foreach ($resource_tags as $resource_tag) {
            $resource_tag->delete();
        }

        $tag = Tag::find($request->tag_id);
        if (count($tag->resources) == 0) {
            $tag->delete();
        }

        return response()->json(['message' => 'Resource Tag deleted Successfully']);
    }
}

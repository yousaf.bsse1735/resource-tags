<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ResourceTagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            [
                "resource_id" => 1,
                "tag_id" => 1,
            ],
            [
                "resource_id" => 1,
                "tag_id" => 2,
            ],
            [
                "resource_id" => 1,
                "tag_id" => 3,
            ],
            [
                "resource_id" => 1,
                "tag_id" => 4,
            ],
            [
                "resource_id" => 2,
                "tag_id" => 1,
            ],
            [
                "resource_id" => 2,
                "tag_id" => 2,
            ],
            [
                "resource_id" => 2,
                "tag_id" => 7,
            ],
            [
                "resource_id" => 2,
                "tag_id" => 8,
            ],
            [
                "resource_id" => 2,
                "tag_id" => 9,
            ],
            [
                "resource_id" => 2,
                "tag_id" => 10,
            ],
        ];
        DB::table('resource_tag')->insert($data);
    }
}

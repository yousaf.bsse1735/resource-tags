<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TagsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            [
                "title" => "sale"
            ],
            [
                "title" => "new"
            ],
            [
                "title" => "popular"
            ],
            [
                "title" => "2023"
            ],
            [
                "title" => "action"
            ],
            [
                "title" => "comedy"
            ],
            [
                "title" => "fiction"
            ],
            [
                "title" => "technology"
            ],
            [
                "title" => "sports"
            ],
            [
                "title" => "religious"
            ],
        ];
        DB::table('tags')->insert($data);
    }
}

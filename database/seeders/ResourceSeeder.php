<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ResourceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            [
                "title" => "Book"
            ],
            [
                "title" => "Game"
            ]
        ];
        DB::table('resources')->insert($data);
    }
}
